FROM jenkins/jenkins:latest

# Environment Variables
ARG BUILD_ENV=local
ARG CASC=/casc/jenkins-casc.yaml
ARG PLUGINS=/plugins/jenkins.plugins
ARG SEEDER_JOB=/seeder/
ARG JAVA_OPTS

# Install Plugins
COPY --chown=jenkins:jenkins ${PLUGINS} ${REF}/plugins.txt
RUN jenkins-plugin-cli -f ${REF}/plugins.txt

# Configuration as Code File
RUN mkdir ${REF}/casc_configs
COPY --chown=jenkins:jenkins ${CASC} ${REF}/casc_configs/jenkins.yaml

# Copy Jobs
RUN mkdir ${REF}/Jobs
COPY --chown=jenkins:jenkins ${SEEDER_JOB} ${REF}/jobs

# Values set in container when running
ENV JAVA_OPTS "-Djenkins.install.runSetupWizard=false ${JAVA_OPTS:-}"
ENV CASC_JENKINS_CONFIG ${REF}/casc_configs
ENV BUILD_ENV ${BUILD_ENV}